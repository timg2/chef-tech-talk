# HelloWorld

A HelloWorld introduction to Chef


1. You can toggle the includes within the recipe default.rb to use either file or template resources
    Watch the unit test fail due to the create_template vs create_file resource change

2. You can toggle .kitchen.yml to use either the role based or recipe based run list
   The overrides at the role level will cause the unit test to fail, this is demonstrating
   how to override attributes
   Note: .kitchen.yml points to the roles directory, this is manual confguration for the chef_zero runner





LIVE demo
# Create the cookbook
chef generate cookbook HelloWorld

# Modify the unit test (remove centos, ubuntu 14.04)
it 'creates the HelloWorld user'
it 'creates the HelloWorld.txt file'

# run the spec test to show the pending tests

# update the tests with the real test content, and watch them fail
it 'has the hello world user' do
  expect(chef_run).to create_user('HelloWorld').with( {
      shell: '/bin/bash',
      home: "/home/HelloWorld" }
  )
end

it 'creates the HelloWorld file' do
  expect(chef_run).to create_template("/home/HelloWorld/HelloWorld.txt")
end

# update the HelloWorld default recipe
# step 1 Create a new user called HelloWorld
# step 2 Put a file in their home directory called HelloWorld.txt
user "HelloWorld" do
  home '/home/HelloWorld'
  password 'password'
  shell '/bin/bash'
  manage_home true
  action :create
end

file "/home/HelloWorld/HelloWorld.txt" do
  content 'HelloWorld!'
  mode 0666
  owner 'HelloWorld'
  action :create
end


# Run the test again and watch it pass

# talk about kitchen and run kitchen converge

# update the smoke test
describe user('HelloWorld') do
  it { should exist }
end

describe file('/home/HelloWorld/HelloWorld.txt') do
  its('content') { should match /HelloWorld/ }
end

# run kitchen verify

# we wrote a unit test, and we wrote an integration test that
# ran on the node.

# We don't want the content HelloWorld.txt to be the same everywhere
# we need some attributes

# generate attributes
chef generate attribute default
chef generate template default

# paste this in default attribute
 default['HelloWorld']['message'] = 'HelloWorld from template!'

# paste this in HelloWorld.erb
<%= node['HelloWorld']['message'] %>
<%= node['fqdn'] %>

# change file in recipe to
template "/home/HelloWorld/HelloWorld.txt" do
  mode 0666
  owner 'HelloWorld'
  action :create
end

# run kitchen converge, run kitchen verify
# watch the tests pass

# theres an attribute hierarchy, this is one way to override default attributes
# so we are going to create what's known as a role
# a role contains a runlist and attributes

# create cookbooks/roles folder
# create helloworld.json and paste
{
  "name": "helloworld",
  "default_attributes": {
      "HelloWorld": {
        "message" : "Override my message!"
      }
    }
  ,
  "run_list": [
    "recipe[HelloWorld::default]"
  ]
}

#modify kitchen.yml
- role[helloworld
