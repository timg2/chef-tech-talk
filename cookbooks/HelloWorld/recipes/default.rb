#
# Cookbook:: HelloWorld
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

include_recipe 'HelloWorld::user'
include_recipe 'HelloWorld::hello_world_file'
