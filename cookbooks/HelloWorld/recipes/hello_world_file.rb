#
# Cookbook:: HelloWorld
# Recipe:: hello_world_file
#
# Copyright:: 2018, The Authors, All Rights Reserved.

file "/home/HelloWorld/HelloWorld.txt" do
  content 'HelloWorld!'
  mode 0666
  owner 'HelloWorld'
end
