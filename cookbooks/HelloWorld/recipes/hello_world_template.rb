#
# Cookbook:: HelloWorld
# Recipe:: hello_world_template
#
# Copyright:: 2018, The Authors, All Rights Reserved.
user_path = node['HelloWorld']['user-path']


template "#{user_path}/HelloWorld.txt" do
  source 'HelloWorld.erb'
  mode '0666'
  owner 'HelloWorld'
end
