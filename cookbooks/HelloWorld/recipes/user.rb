#
# Cookbook:: HelloWorld
# Recipe:: user
#
# Copyright:: 2018, The Authors, All Rights Reserved.
user_path = node['HelloWorld']['user-path']

user "HelloWorld" do
  home user_path
  password 'password'
  shell '/bin/bash'
  manage_home true
end
