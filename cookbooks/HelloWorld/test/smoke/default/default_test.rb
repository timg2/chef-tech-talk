describe user('HelloWorld') do
  it { should exist }
end

describe file('/home/HelloWorld/HelloWorld.txt') do
  its('content') { should match /HelloWorld/ }
end
